from pyquery import PyQuery as pq
import pickle
import os.path

if os.path.isfile("proxies.p"):
    with open("proxies.p", "rb") as f:
        existing_proxies = pickle.load(f)
else:
    existing_proxies = set()

d = pq(url="https://www.sslproxies.org/")

rows = d("#proxylisttable > tbody > tr")

proxies = set()

for r in rows:
    row = pq(r)
    tds = row.find("td")
    ip = tds[0].text
    port = tds[1].text
    anonymity = tds[4].text
    https = tds[6].text

    if https == 'yes':
        proxies.add((ip, port))

print(proxies)
print(len(proxies))

proxies = proxies | existing_proxies

with open("proxies.p", "wb") as f:
    pickle.dump(proxies, f)
