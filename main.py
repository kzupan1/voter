from lxml import etree
import codecs
import pickle

def calc_percentage(arg):
    total = sum(x for _, x in arg)

    return [(x, y/total) for x, y in arg]

def calc_cumulative_percentage(arg):
    cumulative_percentage = []
    cs = 0.0

    for x, y in arg:
        cs += y
        cumulative_percentage.append((x, cs))

    return cumulative_percentage

names = codecs.open("names.xml", "r", "utf-8")
lnames = codecs.open("last_names.xml", "r", "utf-8")

doc1 = etree.parse(names)
doc2 = etree.parse(lnames)

root1 = doc1.getroot()
root2 = doc2.getroot()

men = []
women = []
last_names =[]

for tr in root1.iter('tr'):
    tds = tr.findall('td')
    men.append((tds[1].text.strip(), int(tds[2].text.strip())))
    women.append((tds[4].text.strip(), int(tds[5].text.strip())))

for tr in root2.iter('tr'):
    tds = tr.findall('td')
    last_names.append((tds[1].text.strip(), int(tds[2].text.strip())))

print(men)
print(women)
print(last_names)

men_perc = calc_percentage(men)
women_perc = calc_percentage(women)
last_names_perc = calc_percentage(last_names)

men_cp = calc_cumulative_percentage(men_perc)
women_cp = calc_cumulative_percentage(women_perc)
last_names_cp = calc_cumulative_percentage(last_names_perc)

print(men_cp)
print(women_cp)
print(last_names_cp)

pickle.dump(men_cp, open("men_cp.p", "wb"))
pickle.dump(women_cp, open("women_cp.p", "wb"))
pickle.dump(last_names_cp, open("last_names_cp.p", "wb"))


