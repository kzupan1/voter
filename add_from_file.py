import pickle
import os

if os.path.isfile("proxies.p"):
    with open("proxies.p", "rb") as f:
        existing_proxies = pickle.load(f)

else:
    existing_proxies = set()

proxies = set()

with open("proxy.txt", "r") as f:
    content = f.readlines()
    for line in content:
        l = line.strip()
        proxy = l.split(":")
        proxies.add((proxy[0], proxy[1]))


print(proxies)


proxies = proxies | existing_proxies

print(len(proxies))

with open("proxies.p", "wb") as f:
    pickle.dump(proxies, f)