import pickle
import urllib
from pyquery import PyQuery as pq

places = pickle.load(open("places.p", "rb"))
address = set()
#places = ["http://www.itis.si/kraj/Sv.-Andrej"]

for page_base in places:
    print(page_base)

    page_url = page_base + "/stran-1"
    while True:
        try:
            d1 = pq(url=page_url)
            print(page_url)

            for i in d1(".grid > div > div> div.info > div.contacts > div.location"):
                splitted = pq(i).html().strip().split('<span class="marked"/>')
                hn = splitted[0].replace(",", "").strip()
                city = splitted[1].replace("<br/>", "").strip()
                post = splitted[2].replace("&#13;", "").strip().split(" ", 1)
                address.add((hn, city, post[0], post[1]))

            if not d1('.next-last a#ctl00_CPH_bodyMain_SearchResultsStatic1_ResultsPagerStatic1_aNextPage').attr("href"):
                break

            page = d1('.next-last a#ctl00_CPH_bodyMain_SearchResultsStatic1_ResultsPagerStatic1_aNextPage').attr("href").split("/")[-1]

            page_url_new = page_base + "/" + page
            if page_url_new == page_url:
                break
            else:
                page_url = page_url_new
                d1 = pq(url=page_url)
        except Exception as err:
            print("Exception: {0}".format(err))
            break

print(address)
address = list(address)
pickle.dump(address, open("address.p", "wb"))


