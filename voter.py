import pickle
from pyquery import PyQuery as pq
import requests
from collections import OrderedDict
import math
import random
import time
import datetime
from requests_toolbelt.utils import dump
import shutil

def nextTime(rateParameter):
    return -math.log(1.0 - random.random()) / rateParameter

ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%y%m%d%H%M%S')

shutil.copyfile('./used_proxies.p', './used_backup/used_proxies_' + st + '.p')

last_names = pickle.load(open("last_names_cp.p", "rb"))
men = pickle.load(open("men_cp.p", "rb"))
women = pickle.load(open("women_cp.p", "rb"))
address = pickle.load(open("address.p", "rb"))

proxies = pickle.load(open("proxies.p", "rb"))
used_proxies = pickle.load(open("used_proxies.p", "rb"))
ips = set(i for i, j in used_proxies)


def get_name(cp, r):
    for n, p in cp:
        if r < p:
            return n


def generate_random_voter(m, w, ln, a):
    g = w
    if random.random() > 0.5:
        g = m

    name = get_name(g, random.random())
    last_name = get_name(ln, random.random())
    addr = random.choice(a)

    return name, last_name, addr

def replace_invalid_chars(name):
    name = name.replace("č", "c")
    name = name.replace("ž", "z")
    name = name.replace("š", "s")
    name = name.replace("Č", "C")
    name = name.replace("Ž", "Z")
    name = name.replace("Š", "S")
    name = name.replace("đ", "d")
    name = name.replace("Đ", "D")
    return name

pages = ["https://app.adriadigital.si/slovenski-avto-leta/2017/avtomobilnost-in-val202/",
         "https://app.adriadigital.si/slovenski-avto-leta/2017/motorevija/",
         "https://app.adriadigital.si/slovenski-avto-leta/2017/siol/",
         "https://app.adriadigital.si/slovenski-avto-leta/2017/am/"]


def vote(page, voter, proxies, proxy):

    try:
        get_headeers = OrderedDict()
        get_headeers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        get_headeers['Accept-Encoding'] = 'gzip, deflate, sdch, br'
        get_headeers['Accept-Language'] = 'en-GB,en;q=0.8,en-US;q=0.6,hr;q=0.4,sk;q=0.2,sl;q=0.2,de;q=0.2'
        get_headeers['Cache - Control'] = 'no - cache'
        get_headeers['Pragma'] = 'no - cache'
        get_headeers['Upgrade-Insecure-Requests'] = '1'
        get_headeers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'

        get = requests.get(page, headers=get_headeers, proxies=proxies, timeout=300)
        print(get.status_code, get.reason)

        if get.status_code != 200:
            return

        d = pq(get.content)
        csfrmiddlewaretoken = d("[name='csrfmiddlewaretoken']").val()
        medij = d("[name='medij']").val()
        glasovanje = d("[name='glasovanje']").val()
        odgovor = d("[name='odgovor']").val()
        veljavna = d("[name='veljavna']").val()
        print(voter)
        print(csfrmiddlewaretoken)
        print(medij)
        print(glasovanje)
        print(odgovor)
        print(veljavna)

        data = OrderedDict()
        data["csrfmiddlewaretoken"] = csfrmiddlewaretoken
        data["medij"] = medij
        data["glasovanje"] = glasovanje
        data["odgovor"] = odgovor
        data["veljavna"] = veljavna
        data["odgovor1"] = 9
        data["avto"] = 124
        data["ime"] = voter[0]
        data["priimek"] = voter[1]
        data["email"] = replace_invalid_chars(voter[0].lower()) + "." + replace_invalid_chars(voter[1].lower()) + "@gmail.com"
        data["ulica"] = voter[2][0]
        data["posta"] = voter[2][2]
        data["kraj"] = voter[2][3]
        data["optin"] = "on"

        post_headeers = OrderedDict()
        post_headeers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        post_headeers['Accept-Encoding'] = 'gzip, deflate, sdch, br'
        post_headeers['Accept-Language'] = 'en-GB,en;q=0.8,en-US;q=0.6,hr;q=0.4,sk;q=0.2,sl;q=0.2,de;q=0.2'
        post_headeers['Cache - Control'] = 'no - cache'
        post_headeers['Pragma'] = 'no - cache'
        post_headeers['Referer'] = page
        post_headeers['Upgrade-Insecure-Requests'] = '1'
        post_headeers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'

        print(data)
        slpTime = nextTime(1/30.)
        print("going for a short sleep:" + str(slpTime))
        time.sleep(slpTime)
        print("awake!")
        post = requests.post(page, data=data, cookies={'csrftoken': csfrmiddlewaretoken}, headers=post_headeers, proxies=proxies, timeout=300)
        print(post.status_code, post.reason)

        used_proxies.add(proxy)
        ips.add(proxy[0])
        print("writing...")
        with open("used_proxies.p", "wb") as f:
            pickle.dump(used_proxies, f)

        sleepTime = nextTime(1 / 120.)
        print("going for a big sleep:" + str(sleepTime))
        time.sleep(sleepTime)


    except Exception as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
        print(proxies)

#vote(pages[0], generate_random_voter(men, women, last_names, address))


valid_proxies = [(i, j) for i, j in proxies if i not in ips]
print("valid_proxies:" + str(len(valid_proxies)))

i = 0
for proxy in valid_proxies:
    if proxy[0] in ips:
        continue

    voter = generate_random_voter(men, women, last_names, address)
    page = random.choice(pages)
    proxy_string = "https://" + proxy[0] + ":" + proxy[1]
    print(proxy_string)
    proxies = {
        'http': proxy_string,
        'https': proxy_string
    }

    vote(page, voter, proxies, proxy)

